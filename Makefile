###################### Variables changes ########################
# General files
MAIN = wbp
BIB = biblio
CSL = apa6

# Project name
PROJECT = 'output'

# Date
DATE = $(shell date +"%d%b%Y")

# including pictures and other input (in folder images)
FIGURES := $(shell find images/* -type f)

# All File sources in TeX-Ordner
TEX_SOURCES = Makefile \
	$(MAIN).tex \
		section1.tex \
		section2.tex \
		$(BIB).bib   
################################################################

# GENERAL COMMENTS: CALL MAKE OR MAKE WORD!
# CHANGE VARIABLES ABOVE FIRST! ADD TEX_SOURCES IF NECESSARY!

all: $(MAIN).pdf clean

# SHELL = /bin/bash/ #not sure about it

# only compile without citation or structuring
once:
	xelatex $(MAIN)

# compiling everything
$(MAIN).pdf: $(TEX_SOURCES) $(FIGURES)
	xelatex $(MAIN)
	biber $(MAIN)
	xelatex $(MAIN)
	xelatex $(MAIN)

# cleaning helper documents
clean:
	-rm $(MAIN).log
	-rm $(MAIN).aux
	-rm ${MAIN}.out
	-rm ${MAIN}.bbl
	-rm ${MAIN}.blg
	-rm ${MAIN}.bcf
	-rm ${MAIN}.run.xml

# creating zip-file
zip:
	$(MAKE) clean
	$(MAKE) all
	$(MAKE) clean
	zip -q $(PROJECT)_$(DATE).zip $(TEX_SOURCES) $(FIGURES)

# creating docx with pandoc (copied csl file)
word:
	xelatex $(MAIN)
	biber $(MAIN)
	xelatex $(MAIN)
	xelatex $(MAIN)
	pandoc --citeproc --bibliography=$(BIB).bib --csl=$(CSL).csl $(MAIN).tex -f latex -t docx -o $(MAIN).docx

# creating pictures from tikz-standalone
png: 
	latex $(MAIN)
	convert -density 400 $(MAIN).pdf $(MAIN).png

# secure pdf output file
.PHONY: output.pdf
