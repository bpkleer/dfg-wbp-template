## Walter Benjamin application (2022-11)

This LaTeX document is up to date for Walter Benjamin Programme (2022/11). For adjustments please check: https://www.dfg.de/foerderung/programme/einzelfoerderung/walter_benjamin/

## Credits
This template is based on the [general DFG template of Martin Hölzer](https://github.com/hoelzer/dfg).

## Using the file
 
The main file is `wbp.tex`. All other texts will be included via `\input`. Always open the whole project via the document `wbp.tex`.

All packages calls appear in `header.tex`. Style guides are in `proposal.sty`. 

The document compiles with `XeLaTeX`. It uses **APA** citation guides. 

## Citations
All citations should be saved in `biblio.bib`. In addition the most related works should be have the keyword `3top`. If they have another keyword, change the key in `printbibliography` under 1.2.

## Colors in work packages
To better indicate differences between work packages, I chose different colors. Colors 1 to 3 are defined in `wbp.tex`. Color definitions can be changed or added (if there is an additional work package).

## Todonotes
You can put todonotes in your text via `\todo[inline]{text}` or `\todo{text}`.

## Calling make
If you call make via terminal, it will run `XeLaTeX -> Biber -> XeLaTeX -> XeLaTeX`.

You can also just run `make once` and it will run only `XeLaTeX` once (e.g., when you don't changed any citations).

Additionally you can call `make zip` and `make word`. When you call `make zip` a zip-file will be generated out of the files. `make word` generates a word-document (with pandoc compilation) to share to colleagues who are still working with Word. 